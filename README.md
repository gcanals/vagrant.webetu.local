# Machine virtuelle webetu / vagrant
## pour installation sur machine personnelle


### Prérequis :
* machine linux, macOS ou windows (sauf win10 !)

* VirtualBox (4.3.x) et Vagrant (>1.7.4) doivent être installés sur votre machine.

### caractéristiques de la machine virtuelle : 
* host ip : 192.168.56.15
* hostname : webetu.local

* **système** : CentOs/php54

* **apache** :
   * **vhost défaut** : webetu.local
   * **docroot** : /var/www/html -> ./html
   	
	* **vhost pour le dev** : dev.webetu.local www.webetu.local
	* **docroot** :  /var/www/dev --> ./dev

* **mysql** : 
	* 5.5 - root/123
	* **accès** : webetu.local/adminer
	* acès depuis la machine hôte avec Sequel Pro ou Mysql Workbench : 
		* connexion avec un tunnel ssh, 
			* username : `vagrant`
			* ssh key : `puphpet/files/dot/ssh/id_rsa` (clé générée **après** démarrage de la machine)
		  

* **mongodb**: installé avec php-mongo

### installation, création, démarrage de la machine

* dans votre répertoire de développement, cloner le dépôt :
~~~
git clone https://gcanals@bitbucket.org/gcanals/vagrant.webetu.local.git
~~~

* dans le répertoire créé, créer et démarrer la machine virtuelle avec vagrant : 
~~~
$vagrant up  
~~~

* si tout se passe bien la machine est créée et lancée

### utilisation

* déclarer les hosts dans le fichier /etc/hosts (ou \System32\drivers\etc\hosts ) en ajoutant la ligne : 
~~~
192.168.56.15   webetu.local dev.webetu.local www.webetu.local
~~~

* modifier la configuration réseau de votre navigateur pour éviter d'utiliser le proxy sur webetu.local et *.webetu.local

* tester : l'url dev.webetu.local/info.php doit retourner la configuration php

 
